package net.tncy.bookmarket.service;

import java.util.List;

class BookstoreService {
	
	public int createBookstore(String name) {
		
	}

	public void deleteBookstore(int bookstoreId) {
		
	}

	public Bookstore getBookstoreById(int bookstoreId) {
		
	}

	public List<Bookstore> findBookstoreByName() {
		
	}

	public void renameBookstore(int bookstoreId, String newName) {

	}

	public void addBookToBookstore(int bookstoreId, int bookId, int qty, float price) {
		
	}

	public float removeBookToBookstore(int bookstoreId, int bookId, int qty) {
		
	}

	public List<InventoryEntry> getBookstoreInventory(int bookstoreId) {
		
	}

	public void getBookstoreCatalog(int bookstoreId) {
		
	}
}