package net.tncy.bookmarket.service;

import java.util.List;

class BookService {

	public int createBook(String title, String publisher, String isbn) {
		
	}

	public void deleteBook(int bookId) {
		
	}

	public Book getBookById(int bookId) {
		
	}

	public List<Book> findBookByTitle(String title) {
		
	}

	public List<Book> findBookByAuthor(String author) {
		
	}

	public List<Book> findBookByISBN(String isbn) {
		
	}
}