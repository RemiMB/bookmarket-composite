package net.tncy.bookmarket.data;

class Book {
	private int id;
	private String title;
	private String author;
	private String publisher;
	private BookFormat format;
	private String isbn;

	public Book(int id,
				String title,
				String author,
				String publisher,
				BookFormat format,
				String isbn) {
		this.id = id;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.format = format;
		this.isbn = isbn;
	}
}