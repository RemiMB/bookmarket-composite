package net.tncy.bookmarket.data;

import java.util.List;
import java.util.ArrayList;

class Bookstore {
	private int id;
	private String name;
	List<InventoryEntry> inventoryEntries;

	public Bookstore(int id, String name) {
		this.id = id;
		this.name = name;
		this.inventoryEntries = new ArrayList<InventoryEntry>();
	}
}